#include <polymesh/Mesh.hh>
#include <polymesh/objects.hh>

#include <typed-geometry/tg.hh>

int main()
{
    using namespace pm;

    Mesh m;
    auto pos = m.vertices().make_attribute<tg::pos3>();

    objects::add_cube(m, [&](vertex_handle v, float x, float y, float z) {
        // assign position
        pos[v] = {x, y, z};
    });
}
