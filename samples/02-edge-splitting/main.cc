#include <polymesh/Mesh.hh>
#include <polymesh/properties.hh>
#include <polymesh/formats.hh>
#include <polymesh/objects.hh>

#include <glm/glm.hpp>

int main()
{
    using namespace pm;

    auto target_edge_length = 0.1f;

    Mesh m;
    auto pos = m.vertices().make_attribute<glm::vec3>();
    load("input.obj", m, pos);

    bool changed;
    do
    {
        changed = false;

        for (auto e : m.edges())
        {
            auto l = edge_length(e, pos);
            if (l > target_edge_length)
            {
                auto h1 = e.halfedgeA();

                auto v0 = h1.vertex_from();
                auto v1 = h1.next().vertex_to();
                auto v2 = h1.vertex_to();
                auto v3 = h1.opposite().next().vertex_to();

                m.edges().remove(e);

                auto v = m.vertices().add();
                pos[v] = mix(pos[v0], pos[v2], 0.5f);

                m.faces().add(v, v1, v0);
                m.faces().add(v, v0, v3);
                m.faces().add(v, v3, v2);
                m.faces().add(v, v2, v1);

                changed = true;
            }
        }
    } while (changed);

    m.compactify();

    save("output.obj", pos);
}
