#include <doctest.hh>

#include <polymesh/algorithms.hh>

#include <typed-geometry/tg-std.hh>

TEST_CASE("test that algorithms compile")
{
    pm::Mesh m;
    auto pos = m.vertices().make_attribute<tg::pos3>();
    auto quadrics = m.vertices().make_attribute<tg::quadric3>();

    return; // only compile test

    pm::decimate_down_to(m, pos, quadrics, 1000);
    pm::triangulate_naive(m);
    pm::deduplicate(m, pos);
    pm::vertex_components(m);
    pm::face_components(m);
    pm::optimize_vertices_for_faces(m);
    pm::make_delaunay(m, pos);
    pm::normalize(pos);
    pm::face_normals(pos);

    pm::split_edges_trimesh(
        m,
        [&](pm::edge_handle e) -> tg::optional<float> {
            auto const l = pm::edge_length(e, pos);
            if (l < 0.1f)
                return {};
            return l;
        },
        [&](pm::vertex_handle v, pm::halfedge_handle, pm::vertex_handle v_from, pm::vertex_handle v_to) { pos[v] = mix(pos[v_to], pos[v_from], 0.5f); });
}
