#include <doctest.hh>

#include <polymesh/Mesh.hh>
#include <polymesh/algorithms/decimate.hh>
#include <polymesh/algorithms/triangulate.hh>
#include <polymesh/objects/cube.hh>

#include <typed-geometry/tg.hh>
#include <typed-geometry/feature/quadric.hh>

#include <glow-extras/viewer/view.hh>

TEST_CASE("decimate")
{
    pm::Mesh m;
    auto pos = m.vertices().make_attribute<tg::pos3>();
    pm::objects::add_cube(m, pos);
    pm::triangulate_naive(m);
    m.compactify();

    auto fnormals = pm::triangle_normals(pos);
    auto errors = m.vertices().map([&](pm::vertex_handle v) {
        auto p = pos[v];
        return v.faces().avg([&](pm::face_handle f) {
            auto n = fnormals[f];
            return tg::probabilistic_plane_quadric(p, n, 0.05f, 0.05f);
        });
    });

    pm::decimate_config<tg::pos3, tg::quadric3> cfg;
    cfg.max_error = 0.2f;
    cfg.max_normal_dev = 0.05f;
    pm::decimate(m, pos, errors, cfg);

    // auto v = view(pos);
    // view(lines(pos).line_width_world(0.01f));
}
