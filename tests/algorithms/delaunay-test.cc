#include <doctest.hh>

#include <polymesh/Mesh.hh>
#include <polymesh/algorithms/delaunay.hh>
#include <polymesh/algorithms/triangulate.hh>
#include <polymesh/formats.hh>
#include <polymesh/objects/quad.hh>

#include <glm/glm.hpp>

#include <typed-geometry/tg.hh>

TEST_CASE("Algorithms.Delaunay")
{
    pm::Mesh m;
    auto pos = attribute(m.vertices(), glm::vec3{});

    tg::rng rng;

    for (auto i = 0; i < 3; ++i)
    {
        m.clear();

        int cx = 20;
        int cy = 20;
        auto noise = 0.3f;
        auto hnoise = 0.3f;

        pm::objects::add_quad(
            m,
            [&](pm::vertex_handle v, float x, float y) {
                //
                pos[v] = {x * cx + uniform(rng, -noise, noise), uniform(rng, -hnoise, hnoise), y * cy + uniform(rng, -noise, noise)};
            },
            cx, cy);

        triangulate_naive(m);

        pm::make_delaunay(m, pos);
    }

    // save("/tmp/quad.obj", m, pos);
}
