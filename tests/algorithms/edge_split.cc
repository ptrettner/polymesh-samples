#include <doctest.hh>

#include <polymesh/Mesh.hh>
#include <polymesh/algorithms/edge_split.hh>
#include <polymesh/algorithms/triangulate.hh>
#include <polymesh/objects/quad.hh>

#include <typed-geometry/tg.hh>

#include <glow-extras/viewer/view.hh>

TEST_CASE("edge split")
{
    pm::Mesh m;
    auto pos = m.vertices().make_attribute<tg::pos3>();

    tg::rng rng;
    auto s = 8;
    pm::objects::add_quad(
        m,
        [&](auto v, float x, float y) {
            pos[v] = {x * s + uniform(rng, -.2f, .2f), 0, y * s + uniform(rng, -.2f, .2f)};
        },
        s, s);

    pm::triangulate_naive(m);
    m.compactify();

    pm::split_edges_trimesh(
        m,
        [&](pm::edge_handle e) -> tg::optional<float> {
            auto l = distance(pos[e.vertexA()], pos[e.vertexB()]);
            if (l < 0.2f)
                return {};
            return l;
        },
        [&](pm::vertex_handle v, pm::halfedge_handle, pm::vertex_handle v_from, pm::vertex_handle v_to) {
            pos[v] = mix(pos[v_from], pos[v_to], 0.5f);
            // is OK! m.assert_consistency();
        });

    return; // DO NOT ACTUALLY SHOW
    auto v = gv::view(pos);
    gv::view(gv::lines(pos).line_width_world(0.003f));
}

TEST_CASE("edge tri_split")
{
    pm::Mesh m;
    auto v00 = m.vertices().add();
    auto v10 = m.vertices().add();
    auto v11 = m.vertices().add();
    auto v01 = m.vertices().add();

    auto f0 = m.faces().add(v00, v01, v10);
    auto f1 = m.faces().add(v11, v10, v01);

    m.assert_consistency();

    {
        auto e = pm::edge_between(v01, v10);
        CHECK(e.is_valid());
        auto v = m.edges().split_and_triangulate(e);

        CHECK(v.is_valid());
        CHECK(pm::edge_between(v, v00).is_valid());
        CHECK(pm::edge_between(v, v01).is_valid());
        CHECK(pm::edge_between(v, v10).is_valid());
        CHECK(pm::edge_between(v, v11).is_valid());
        CHECK(m.faces().size() == 4);
        CHECK(m.vertices().size() == 5);
        CHECK(m.edges().size() == 8);
        m.assert_consistency();
    }

    {
        auto e = pm::edge_between(v00, v01);
        CHECK(e.is_valid());
        auto vv = m.edges().split_and_triangulate(e);

        CHECK(vv.is_valid());
        CHECK(m.faces().size() == 5);
        CHECK(m.vertices().size() == 6);
        CHECK(m.edges().size() == 10);
        m.assert_consistency();
    }
}