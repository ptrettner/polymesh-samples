#include <doctest.hh>

#include <functional>
#include <string>
#include <vector>

#include <iostream>

#include <polymesh/Mesh.hh>
#include <polymesh/properties.hh>
#include <polymesh/algorithms/triangulate.hh>
#include <polymesh/objects/cube.hh>

#include <typed-geometry/feature/random.hh>

TEST_CASE("topo fuzzer")
{
    return; // TODO: enable all, fix segfault

    for (auto _ = 0; _ < 20; ++_)
    {
        tg::rng rng;
        pm::Mesh m;
        pm::objects::add_cube(m, [](auto&&...) {});
        pm::triangulate_naive(m);

        std::vector<std::pair<std::string, std::function<void()>>> ops;

        auto gen_permutation = [&](size_t s) {
            std::vector<int> p;
            p.resize(s);
            for (auto i = 0; i < int(s); ++i)
                p[i] = i;
            std::shuffle(p.begin(), p.end(), rng);
            return p;
        };

        // vertex ops
        ops.emplace_back("add vertex", [&] {
            m.vertices().add(); //
        });
        ops.emplace_back("collapse vertex", [&] {
            if (m.vertices().empty())
                return;
            auto v = m.vertices().random(rng);
            if (v.is_boundary())
                return; // TODO: implement me
            m.vertices().collapse(v);
        });
        ops.emplace_back("remove vertex", [&] {
            if (m.vertices().empty())
                return;
            auto v = m.vertices().random(rng);
            m.vertices().remove(v);
        });
        ops.emplace_back("permute vertices", [&] {
            m.vertices().permute(gen_permutation(m.all_vertices().size())); //
        });

        // face ops
        ops.emplace_back("split face", [&] {
            if (m.faces().empty())
                return;
            auto f = m.faces().random(rng);
            m.faces().split(f);
        });
        // TODO: properly implement can_add
        // ops.emplace_back("add face (triangle)", [&] {
        //     if (m.vertices().empty())
        //         return;
        //     auto v0 = m.vertices().random(rng);
        //     auto v1 = m.vertices().random(rng);
        //     auto v2 = m.vertices().random(rng);
        //     if (m.faces().can_add(v0, v1, v2))
        //         m.faces().add(v0, v1, v2);
        // });
        // ops.emplace_back("add face (quad)", [&] {
        //     if (m.vertices().empty())
        //         return;
        //     auto v0 = m.vertices().random(rng);
        //     auto v1 = m.vertices().random(rng);
        //     auto v2 = m.vertices().random(rng);
        //     auto v3 = m.vertices().random(rng);
        //     if (m.faces().can_add(v0, v1, v2, v3))
        //         m.faces().add(v0, v1, v2, v3);
        // });
        ops.emplace_back("fill face", [&] {
            if (m.halfedges().empty())
                return;
            auto h = m.halfedges().random(rng);
            if (h.is_boundary())
                m.faces().fill(h);
        });
        ops.emplace_back("remove face", [&] {
            if (m.faces().empty())
                return;
            auto f = m.faces().random(rng);
            m.faces().remove(f);
        });
        ops.emplace_back("permute faces", [&] {
            m.faces().permute(gen_permutation(m.all_faces().size())); //
        });

        // edge ops
        // TODO: fixme!
        // ops.emplace_back("split edge", [&] {
        //     if (m.edges().empty())
        //         return;
        //     auto e = m.edges().random(rng);
        //     m.edges().split(e);
        // });
        ops.emplace_back("flip edge", [&] {
            if (m.edges().empty())
                return;
            auto e = m.edges().random(rng);
            if (can_flip(e))
                m.edges().flip(e);
        });
        ops.emplace_back("rotate edge next", [&] {
            if (m.edges().empty())
                return;
            auto e = m.edges().random(rng);
            if (can_rotate_next(e))
                m.edges().rotate_next(e);
        });
        ops.emplace_back("rotate edge prev", [&] {
            if (m.edges().empty())
                return;
            auto e = m.edges().random(rng);
            if (can_rotate_prev(e))
                m.edges().rotate_prev(e);
        });
        ops.emplace_back("remove edge", [&] {
            if (m.edges().empty())
                return;
            auto e = m.edges().random(rng);
            m.edges().remove(e);
        });
        ops.emplace_back("premute edges", [&] {
            m.edges().permute(gen_permutation(m.all_edges().size())); //
        });

        // halfedge ops
        ops.emplace_back("rotate halfedge next", [&] {
            if (m.halfedges().empty())
                return;
            auto h = m.halfedges().random(rng);
            if (can_rotate_next(h))
                m.halfedges().rotate_next(h);
        });
        ops.emplace_back("rotate halfedge prev", [&] {
            if (m.halfedges().empty())
                return;
            auto h = m.halfedges().random(rng);
            if (can_rotate_prev(h))
                m.halfedges().rotate_prev(h);
        });

        // general
        ops.emplace_back("compactify", [&] {
            m.compactify(); //
        });

        for (auto i = 0; i < 100; ++i)
        {
            auto const& op = uniform(rng, ops);
            // std::cout << op.first << std::endl;
            op.second();
            m.assert_consistency();
        }
    }
}
