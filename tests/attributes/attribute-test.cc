#include <doctest.hh>

#include <polymesh/Mesh.hh>

// size tests:
static_assert(sizeof(pm::vertex_attribute<bool>)
                  == (sizeof(void*) +     // pointer to mesh
                      sizeof(void*) +     // pointer to data
                      2 * sizeof(void*) + // pointer to next/prev attr
                      sizeof(uint64_t) +  // default value
                      sizeof(uint64_t)    // empty base class -> alignment
                      ),
              "attribute is bigger than expected");

/*
 *
    // 16 byte
    std::unique_ptr<halfedge_index[]> mFaceToHalfedge;
    int mFacesSize = 0;
    int mFacesCapacity = 0;

    // 16 byte
    std::unique_ptr<halfedge_index[]> mVertexToOutgoingHalfedge;
    int mVerticesSize = 0;
    int mVerticesCapacity = 0;

    // 5 * 8 = 40 byte
    std::unique_ptr<vertex_index[]> mHalfedgeToVertex;
    std::unique_ptr<face_index[]> mHalfedgeToFace;
    std::unique_ptr<halfedge_index[]> mHalfedgeToNextHalfedge;
    std::unique_ptr<halfedge_index[]> mHalfedgeToPrevHalfedge;
    int mHalfedgesSize = 0;
    int mHalfedgesCapacity = 0;

    // 16 byte
    bool mCompact = true;
    int mRemovedFaces = 0;
    int mRemovedVertices = 0;
    int mRemovedHalfedges = 0;

    // 4 x 8 = 32 byte
    // linked lists of all attributes
    mutable primitive_attribute_base<vertex_tag> *mVertexAttrs = nullptr;
    mutable primitive_attribute_base<face_tag> *mFaceAttrs = nullptr;
    mutable primitive_attribute_base<edge_tag> *mEdgeAttrs = nullptr;
    mutable primitive_attribute_base<halfedge_tag> *mHalfedgeAttrs = nullptr;
 */
static_assert(sizeof(pm::Mesh) == 120, "mesh is bigger than expected");

TEST_CASE("Attributes.Creation")
{
    pm::Mesh m;

    auto v0 = m.vertices().add();
    auto v1 = m.vertices().add();
    auto v2 = m.vertices().add();

    auto f = m.faces().add(v0, v1, v2);

    {
        auto a0 = m.vertices().make_attribute<int>();
        auto a1 = m.vertices().make_attribute(3);
        auto a2 = m.vertices().make_attribute_from_data(std::vector<int>(3, 5));
        auto a3 = m.vertices().map([](auto) { return 7; });
        auto a4 = m.vertices().map([](auto) { return 11; });
        auto a5 = pm::vertex_attribute<int>(m);
        // TODO: auto a6 = pm::vertex_attribute(m, 13);
        auto a7 = pm::attribute<int>(m.vertices());
        auto a8 = attribute(m.vertices(), 15);

        for (auto v : {v0, v1, v2})
        {
            CHECK(a0[v] == 0);
            CHECK(a1[v] == 3);
            CHECK(a2[v] == 5);
            CHECK(a3[v] == 7);
            CHECK(a4[v] == 11);
            CHECK(a5[v] == 0);
            // CHECK(a6[v] == 13);
            CHECK(a7[v] == 0);
            CHECK(a8[v] == 15);
        }
    }

    {
        auto a0 = m.faces().make_attribute<int>();
        auto a1 = m.faces().make_attribute(3);
        auto a2 = m.faces().make_attribute_from_data(std::vector<int>(3, 5));
        auto a3 = m.faces().map([](auto) { return 7; });
        auto a4 = m.faces().map([](auto) { return 11; });
        auto a5 = pm::face_attribute<int>(m);
        // TODO: auto a6 = pm::face_attribute(m, 13);
        auto a7 = pm::attribute<int>(m.faces());
        auto a8 = attribute(m.faces(), 15);

        CHECK(a0[f] == 0);
        CHECK(a1[f] == 3);
        CHECK(a2[f] == 5);
        CHECK(a3[f] == 7);
        CHECK(a4[f] == 11);
        CHECK(a5[f] == 0);
        // CHECK(a6[f] == 13);
        CHECK(a7[f] == 0);
        CHECK(a8[f] == 15);
    }

    {
        auto a0 = m.edges().make_attribute<int>();
        auto a1 = m.edges().make_attribute(3);
        auto a2 = m.edges().make_attribute_from_data(std::vector<int>(3, 5));
        auto a3 = m.edges().map([](auto) { return 7; });
        auto a4 = m.edges().map([](auto) { return 11; });
        auto a5 = pm::edge_attribute<int>(m);
        // TODO: auto a6 = pm::edge_attribute(m, 13);
        auto a7 = pm::attribute<int>(m.edges());
        auto a8 = attribute(m.edges(), 15);
    }

    {
        auto a0 = m.halfedges().make_attribute<int>();
        auto a1 = m.halfedges().make_attribute(3);
        auto a2 = m.halfedges().make_attribute_from_data(std::vector<int>(3, 5));
        auto a3 = m.halfedges().map([](auto) { return 7; });
        auto a4 = m.halfedges().map([](auto) { return 11; });
        auto a5 = pm::halfedge_attribute<int>(m);
        // TODO: auto a6 = pm::halfedge_attribute(m, 13);
        auto a7 = pm::attribute<int>(m.halfedges());
        auto a8 = attribute(m.halfedges(), 15);
    }
}

TEST_CASE("Attributes.Registry")
{
    pm::Mesh m;
    auto ll = low_level_api(m);

    auto ASSERT_ATTR_CNT = [&](int v, int f, int e, int h) {
        CHECK(ll.vertex_attribute_count() == v);
        CHECK(ll.face_attribute_count() == f);
        CHECK(ll.edge_attribute_count() == e);
        CHECK(ll.halfedge_attribute_count() == h);
    };

    ASSERT_ATTR_CNT(0, 0, 0, 0);

    {
        auto a = m.vertices().make_attribute<int>();

        ASSERT_ATTR_CNT(1, 0, 0, 0);

        {
            auto b = m.vertices().make_attribute<int>();

            ASSERT_ATTR_CNT(2, 0, 0, 0);

            auto a0 = m.faces().make_attribute<int>();
            auto a1 = m.halfedges().make_attribute<int>();
            auto a2 = m.edges().make_attribute<int>();

            ASSERT_ATTR_CNT(2, 1, 1, 1);
        }

        ASSERT_ATTR_CNT(1, 0, 0, 0);
    }

    ASSERT_ATTR_CNT(0, 0, 0, 0);

    {
        auto a = m.vertices().make_attribute<int>();
        auto b = m.vertices().make_attribute<int>();

        ASSERT_ATTR_CNT(2, 0, 0, 0);

        b = a; // copy

        ASSERT_ATTR_CNT(2, 0, 0, 0);

        b = std::move(a); // move
        CHECK(!a.is_valid());

        ASSERT_ATTR_CNT(1, 0, 0, 0);

        auto c = m.vertices().make_attribute<int>();

        ASSERT_ATTR_CNT(2, 0, 0, 0);

        pm::vertex_attribute<int> d(m); // "default" ctor

        ASSERT_ATTR_CNT(3, 0, 0, 0);

        pm::vertex_attribute<int> e(std::move(c)); // move c into e
        CHECK(!c.is_valid());

        ASSERT_ATTR_CNT(3, 0, 0, 0);

        pm::vertex_attribute<int> f(d); // copy d into f

        ASSERT_ATTR_CNT(4, 0, 0, 0);
    }

    ASSERT_ATTR_CNT(0, 0, 0, 0);
}

TEST_CASE("Attributes.DefaultSemantic")
{
    pm::Mesh m;

    auto ll = low_level_api(m);

    m.vertices().add();
    m.vertices().add();

    auto a = m.vertices().make_attribute(7);

    CHECK(ll.vertex_attribute_count() == 1);
    for (auto v : m.vertices())
        CHECK(a[v] == 7);

    auto b = m.vertices().make_attribute(9);

    CHECK(ll.vertex_attribute_count() == 2);
    for (auto v : m.vertices())
    {
        CHECK(a[v] == 7);
        CHECK(b[v] == 9);
    }

    b = a;

    CHECK(ll.vertex_attribute_count() == 2);
    for (auto v : m.vertices())
    {
        CHECK(a[v] == 7);
        CHECK(b[v] == 7);
    }

    m.vertices().add();

    CHECK(ll.vertex_attribute_count() == 2);
    for (auto v : m.vertices())
    {
        CHECK(a[v] == 7);
        CHECK(b[v] == 7);
    }

    auto c = b;            // copy
    auto d = std::move(b); // move
    CHECK(!b.is_valid());

    CHECK(ll.vertex_attribute_count() == 3);
    for (auto v : m.vertices())
    {
        CHECK(a[v] == 7);
        CHECK(c[v] == 7);
        CHECK(d[v] == 7);
    }

    auto e = m.vertices().make_attribute(11);
    auto f = m.vertices().make_attribute(13);
    c = std::move(f);
    CHECK(!f.is_valid());

    CHECK(ll.vertex_attribute_count() == 4);
    for (auto v : m.vertices())
    {
        CHECK(a[v] == 7);
        CHECK(c[v] == 13);
        CHECK(d[v] == 7);
        CHECK(e[v] == 11);
    }

    e = std::move(a);
    CHECK(!a.is_valid());

    CHECK(ll.vertex_attribute_count() == 3);
    for (auto v : m.vertices())
    {
        CHECK(c[v] == 13);
        CHECK(d[v] == 7);
        CHECK(e[v] == 7);
    }

    for (auto i = 0; i < 100; ++i)
        m.vertices().add();

    CHECK(ll.vertex_attribute_count() == 3);
    for (auto v : m.vertices())
    {
        CHECK(c[v] == 13);
        CHECK(d[v] == 7);
        CHECK(e[v] == 7);
    }

    {
        m.clear();

        m.vertices().add();
        m.vertices().add();
        m.vertices().add();

        CHECK(ll.vertex_attribute_count() == 3);
        for (auto v : m.vertices())
        {
            CHECK(c[v] == 13);
            CHECK(d[v] == 7);
            CHECK(e[v] == 7);
        }

        c.clear(1);
        d.clear(2);
        e.clear(3);

        CHECK(ll.vertex_attribute_count() == 3);
        for (auto v : m.vertices())
        {
            CHECK(c[v] == 1);
            CHECK(d[v] == 2);
            CHECK(e[v] == 3);
        }
    }

    {
        m.clear();

        m.vertices().add();
        m.vertices().add();
        m.vertices().add();

        CHECK(ll.vertex_attribute_count() == 3);
        for (auto v : m.vertices())
        {
            CHECK(c[v] == 13);
            CHECK(d[v] == 7);
            CHECK(e[v] == 7);
        }

        c.clear(1);
        d.clear(2);
        e.clear(3);

        CHECK(ll.vertex_attribute_count() == 3);
        for (auto v : m.vertices())
        {
            CHECK(c[v] == 1);
            CHECK(d[v] == 2);
            CHECK(e[v] == 3);
        }

        m.shrink_to_fit();

        CHECK(ll.vertex_attribute_count() == 3);
        for (auto v : m.vertices())
        {
            CHECK(c[v] == 1);
            CHECK(d[v] == 2);
            CHECK(e[v] == 3);
        }

        c.clear(4);
        d.clear(5);
        e.clear(6);

        CHECK(ll.vertex_attribute_count() == 3);
        for (auto v : m.vertices())
        {
            CHECK(c[v] == 4);
            CHECK(d[v] == 5);
            CHECK(e[v] == 6);
        }
    }

    {
        m.reset();

        m.vertices().add();
        m.vertices().add();
        m.vertices().add();

        CHECK(ll.vertex_attribute_count() == 3);
        for (auto v : m.vertices())
        {
            CHECK(c[v] == 13);
            CHECK(d[v] == 7);
            CHECK(e[v] == 7);
        }

        c.clear(1);
        d.clear(2);
        e.clear(3);

        CHECK(ll.vertex_attribute_count() == 3);
        for (auto v : m.vertices())
        {
            CHECK(c[v] == 1);
            CHECK(d[v] == 2);
            CHECK(e[v] == 3);
        }
    }
}

TEST_CASE("Attributes.DefaultSemanticEdge")
{
    pm::Mesh m;

    auto ll = low_level_api(m);

    ll.alloc_edge();
    ll.alloc_edge();

    auto a = m.edges().make_attribute(7);

    CHECK(ll.edge_attribute_count() == 1);
    for (auto e : m.edges())
        CHECK(a[e] == 7);

    auto b = m.edges().make_attribute(9);

    CHECK(ll.edge_attribute_count() == 2);
    for (auto e : m.edges())
    {
        CHECK(a[e] == 7);
        CHECK(b[e] == 9);
    }

    b = a;

    CHECK(ll.edge_attribute_count() == 2);
    for (auto e : m.edges())
    {
        CHECK(a[e] == 7);
        CHECK(b[e] == 7);
    }

    ll.alloc_edge();

    CHECK(ll.edge_attribute_count() == 2);
    for (auto e : m.edges())
    {
        CHECK(a[e] == 7);
        CHECK(b[e] == 7);
    }

    auto c = b;            // copy
    auto d = std::move(b); // move
    CHECK(!b.is_valid());

    CHECK(ll.edge_attribute_count() == 3);
    for (auto e : m.edges())
    {
        CHECK(a[e] == 7);
        CHECK(c[e] == 7);
        CHECK(d[e] == 7);
    }

    auto e = m.edges().make_attribute(11);
    auto f = m.edges().make_attribute(13);
    c = std::move(f);
    CHECK(!f.is_valid());

    CHECK(ll.edge_attribute_count() == 4);
    for (auto eh : m.edges())
    {
        CHECK(a[eh] == 7);
        CHECK(c[eh] == 13);
        CHECK(d[eh] == 7);
        CHECK(e[eh] == 11);
    }

    e = std::move(a);
    CHECK(!a.is_valid());

    CHECK(ll.edge_attribute_count() == 3);
    for (auto eh : m.edges())
    {
        CHECK(c[eh] == 13);
        CHECK(d[eh] == 7);
        CHECK(e[eh] == 7);
    }

    for (auto i = 0; i < 100; ++i)
        ll.alloc_edge();

    CHECK(ll.edge_attribute_count() == 3);
    for (auto eh : m.edges())
    {
        CHECK(c[eh] == 13);
        CHECK(d[eh] == 7);
        CHECK(e[eh] == 7);
    }

    m.clear();

    ll.alloc_edge();
    ll.alloc_edge();
    ll.alloc_edge();

    CHECK(ll.edge_attribute_count() == 3);
    for (auto eh : m.edges())
    {
        CHECK(c[eh] == 13);
        CHECK(d[eh] == 7);
        CHECK(e[eh] == 7);
    }

    c.clear(1);
    d.clear(2);
    e.clear(3);

    CHECK(ll.edge_attribute_count() == 3);
    for (auto eh : m.edges())
    {
        CHECK(c[eh] == 1);
        CHECK(d[eh] == 2);
        CHECK(e[eh] == 3);
    }
}
