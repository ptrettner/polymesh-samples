#include <doctest.hh>

#include <polymesh/Mesh.hh>

#include <typed-geometry/tg-lean.hh>

TEST_CASE("attribute views")
{
    pm::Mesh m;
    auto pos = m.vertices().make_attribute<tg::pos3>();

    auto v = m.vertices().add();
    pos[v] = {1, 2, 3};

    auto pos_v = pos.view([](tg::pos3& p) -> tg::vec3& { return reinterpret_cast<tg::vec3&>(p); });

    static_assert(std::is_same_v<decltype(pos_v[v]), tg::vec3&>);

    CHECK(pos.size() == pos_v.size());
    CHECK(pos[v] == tg::pos3(1, 2, 3));
    CHECK(pos_v[v] == tg::vec3(1, 2, 3));

    pos[v] = {2, 3, 4};
    CHECK(pos[v] == tg::pos3(2, 3, 4));
    CHECK(pos_v[v] == tg::vec3(2, 3, 4));

    pos_v[v] = {1, 5, 3};

    CHECK(pos[v] == tg::pos3(1, 5, 3));
    CHECK(pos_v[v] == tg::vec3(1, 5, 3));

    pos_v[v].y = 7;

    CHECK(pos[v] == tg::pos3(1, 7, 3));
    CHECK(pos_v[v] == tg::vec3(1, 7, 3));

    auto pos_vz = pos_v.map([](tg::vec3& v) -> float& { return v.z; });
    static_assert(std::is_same_v<decltype(pos_vz[v]), float&>);

    CHECK(pos_vz[v] == 3);

    pos_vz[v] = 9;

    CHECK(pos[v] == tg::pos3(1, 7, 9));
    CHECK(pos_v[v] == tg::vec3(1, 7, 9));
}
