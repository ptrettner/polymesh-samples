#include <doctest.hh>

#include <polymesh/Mesh.hh>
#include <polymesh/attributes/raw_attribute.hh>

TEST_CASE("RawAttributes.Basics")
{
    pm::Mesh m;
    for (auto i = 0; i < 100; ++i)
        m.vertices().add();

    auto a_raw = pm::raw_vertex_attribute(m, 4);
    auto a_int = a_raw.to<int>();

    for (auto v : m.vertices())
        CHECK(a_int[v] == 0);

    for (auto v : m.vertices())
    {
        CHECK(a_raw[v].size() == 4);
        a_raw[v][0] = std::byte(int(v)); // assumes little endian
    }

    a_int = a_raw.to<int>();
    for (auto v : m.vertices())
        CHECK(a_int[v] == int(v));
}

TEST_CASE("RawAttributes.Registry")
{
    pm::Mesh m;
    auto ll = low_level_api(m);

    auto ASSERT_ATTR_CNT = [&](int v, int f, int e, int h) {
        CHECK(ll.vertex_attribute_count() == v);
        CHECK(ll.face_attribute_count() == f);
        CHECK(ll.edge_attribute_count() == e);
        CHECK(ll.halfedge_attribute_count() == h);
    };

    ASSERT_ATTR_CNT(0, 0, 0, 0);

    {
        auto a = pm::raw_vertex_attribute(m, 4);

        ASSERT_ATTR_CNT(1, 0, 0, 0);

        {
            auto b = pm::raw_vertex_attribute(m, 1);

            ASSERT_ATTR_CNT(2, 0, 0, 0);

            auto a0 = pm::raw_face_attribute(m, 4);
            auto a1 = pm::raw_halfedge_attribute(m, 3);
            auto a2 = pm::raw_edge_attribute(m, 7);

            ASSERT_ATTR_CNT(2, 1, 1, 1);
        }

        ASSERT_ATTR_CNT(1, 0, 0, 0);
    }

    ASSERT_ATTR_CNT(0, 0, 0, 0);

    {
        auto a = pm::raw_vertex_attribute(m, 5);
        auto b = pm::raw_vertex_attribute(m, 9);

        ASSERT_ATTR_CNT(2, 0, 0, 0);

        b = a; // copy

        ASSERT_ATTR_CNT(2, 0, 0, 0);

        b = std::move(a); // move
        CHECK(!a.is_valid());

        ASSERT_ATTR_CNT(1, 0, 0, 0);

        auto c = pm::raw_vertex_attribute(m, 3);

        ASSERT_ATTR_CNT(2, 0, 0, 0);

        pm::raw_vertex_attribute d(m, 11); // "default" ctor

        ASSERT_ATTR_CNT(3, 0, 0, 0);

        pm::raw_vertex_attribute e(std::move(c)); // move c into e
        CHECK(!c.is_valid());

        ASSERT_ATTR_CNT(3, 0, 0, 0);

        pm::raw_vertex_attribute f(d); // copy d into f

        ASSERT_ATTR_CNT(4, 0, 0, 0);
    }

    ASSERT_ATTR_CNT(0, 0, 0, 0);
}
