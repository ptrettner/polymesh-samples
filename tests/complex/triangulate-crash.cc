#include <doctest.hh>

#include <polymesh/Mesh.hh>
#include <polymesh/algorithms/triangulate.hh>

#include <glow-extras/viewer/canvas.hh>

TEST_CASE("triangulate_naive crash")
{
    auto m = pm::Mesh();

    std::vector<pm::vertex_index> vertices;
    for (int i = 0; i < 26; ++i)
        vertices.push_back(m.vertices().add());

    std::vector<pm::vertex_index> vs;
    vs.push_back(vertices[1]);
    vs.push_back(vertices[2]);
    vs.push_back(vertices[3]);
    vs.push_back(vertices[4]);
    vs.push_back(vertices[0]);
    // std::reverse(vs.begin(), vs.end());
    m.faces().add(vs);

    vs.clear();
    vs.push_back(vertices[12]);
    vs.push_back(vertices[13]);
    vs.push_back(vertices[9]);
    // std::reverse(vs.begin(), vs.end());
    m.faces().add(vs);

    vs.clear();
    vs.push_back(vertices[4]);
    vs.push_back(vertices[3]);
    vs.push_back(vertices[22]);
    vs.push_back(vertices[18]);
    // std::reverse(vs.begin(), vs.end());
    m.faces().add(vs);

    vs.clear();
    vs.push_back(vertices[0]);
    vs.push_back(vertices[4]);
    vs.push_back(vertices[23]);
    // std::reverse(vs.begin(), vs.end());
    m.faces().add(vs);

    vs.clear();
    vs.push_back(vertices[18]);
    vs.push_back(vertices[22]);
    vs.push_back(vertices[25]);
    // std::reverse(vs.begin(), vs.end());
    m.faces().add(vs);

    vs.clear();
    vs.push_back(vertices[25]);
    vs.push_back(vertices[22]);
    vs.push_back(vertices[3]);
    vs.push_back(vertices[2]);
    vs.push_back(vertices[1]);
    vs.push_back(vertices[13]);
    vs.push_back(vertices[12]);
    // std::reverse(vs.begin(), vs.end());
    m.faces().add(vs);

    vs.clear();
    vs.push_back(vertices[13]);
    vs.push_back(vertices[1]);
    vs.push_back(vertices[0]);
    vs.push_back(vertices[23]);
    // std::reverse(vs.begin(), vs.end());
    m.faces().add(vs);

    vs.clear();
    vs.push_back(vertices[9]);
    vs.push_back(vertices[25]);
    vs.push_back(vertices[12]);
    // std::reverse(vs.begin(), vs.end());
    m.faces().add(vs);

    m.assert_consistency();
    pm::triangulate_naive(m);

    /*auto pos = m.vertices().make_attribute<tg::pos3>();
    tg::rng rng;
    for (auto v : m.vertices())
        pos[v] = uniform(rng, boundary_of(tg::sphere3::unit));
    for (auto i = 0; i < 20; ++i)
    {
        auto new_pos = pos;
        for (auto v : m.vertices())
        {
            if (v.is_isolated())
                continue;

            auto np = v.adjacent_vertices().avg(pos);
            new_pos[v] = tg::mix(pos[v], np, 0.2f);
        }
        for (auto& p : new_pos)
            p = tg::pos3(normalize(tg::vec3(p)));
        pos = new_pos;
    }

    auto c = gv::canvas();
    for (auto v : m.vertices())
        if (!v.is_isolated())
            c.add_point(pos[v]).label(std::to_string(int(v)));
    for (auto e : m.edges())
    {
        auto p0 = pos[e.vertexA()];
        auto p1 = pos[e.vertexB()];
        if (e.is_boundary())
            c.add_line(p0, p1, tg::color3::red);
        else
            c.add_line(p0, p1);
    }*/
}

TEST_CASE("triangulate_naive crash 2")
{
    auto m = pm::Mesh();
    std::vector<pm::vertex_index> vertices;
    for (int i = 0; i < 9; ++i)
        vertices.push_back(m.vertices().add());

    std::vector<pm::vertex_index> vs;
    vs.push_back(vertices[1]);
    vs.push_back(vertices[2]);
    vs.push_back(vertices[3]);
    vs.push_back(vertices[0]);
    m.faces().add(vs);
    vs.clear();
    vs.push_back(vertices[4]);
    vs.push_back(vertices[5]);
    vs.push_back(vertices[2]);
    vs.push_back(vertices[1]);
    m.faces().add(vs);
    vs.clear();
    vs.push_back(vertices[6]);
    vs.push_back(vertices[3]);
    vs.push_back(vertices[2]);
    vs.push_back(vertices[5]);
    m.faces().add(vs);
    vs.clear();
    vs.push_back(vertices[0]);
    vs.push_back(vertices[3]);
    vs.push_back(vertices[6]);
    vs.push_back(vertices[7]);
    m.faces().add(vs);
    vs.clear();
    vs.push_back(vertices[8]);
    vs.push_back(vertices[7]);
    vs.push_back(vertices[6]);
    vs.push_back(vertices[5]);
    vs.push_back(vertices[4]);
    m.faces().add(vs);
    vs.clear();
    vs.push_back(vertices[7]);
    vs.push_back(vertices[8]);
    vs.push_back(vertices[4]);
    vs.push_back(vertices[1]);
    vs.push_back(vertices[0]);
    m.faces().add(vs);
    vs.clear();
    pm::triangulate_naive(m);

    /*
        {
            auto is_inner_valence2 = [](pm::vertex_handle v) { return !v.is_boundary() && valence(v) == 2; };

            std::vector<pm::vertex_handle> vs;
            for (auto f : m.faces())
            {
                vs.clear();
                f.vertices().into_vector(vs);

                if (vs.size() <= 3)
                    continue;

                // find non-valence 2
                auto si = 0;
                for (auto i = 0u; i < vs.size(); ++i)
                {
                    if (!is_inner_valence2(vs[i]))
                    {
                        si = i;
                        break;
                    }
                }
                POLYMESH_ASSERT(!is_inner_valence2(vs[si]) && "could not find start vertex (second vertex must not be inner valence 2)");
                si--; // make sure v1 is never valence 2
                if (si < 0)
                    si += int(vs.size());

                // remove
                m.faces().remove(f);

                // triangulate
                std::cout << "triangulate " << int(f) << std::endl;
                for (auto i = 2u; i < vs.size(); ++i)
                {
                    auto v0 = vs[si];
                    auto v1 = vs[(si + i - 1) % vs.size()];
                    auto v2 = vs[(si + i) % vs.size()];
                    std::cout << int(v0) << " " << int(v1) << " " << int(v2) << std::endl;
                    for (auto v : {v0, v1, v2})
                        std::cout << "  is inner val 2: " << is_inner_valence2(v) << " (boundary " << v.is_boundary() << ", valence: " << valence(v)
                                  << ")" << std::endl;
                    m.faces().add(v0, v1, v2);
                }
            }
        }*/
}
