#include <doctest.hh>

#include <polymesh/Mesh.hh>

TEST_CASE("Interface.Handles")
{
    pm::Mesh m;

    auto v0 = m.vertices().add();
    auto v1 = m.vertices().add();
    auto v2 = m.vertices().add();

    auto i0 = m.vertices()[0];
    auto i1 = m.vertices()[1];
    auto i2 = m.vertices()[2];

    auto j0 = m.vertices()[pm::vertex_index(0)];
    auto j1 = m.vertices()[pm::vertex_index(1)];
    auto j2 = m.vertices()[pm::vertex_index(2)];

    auto k0 = m[pm::vertex_index(0)];
    auto k1 = m[pm::vertex_index(1)];
    auto k2 = m[pm::vertex_index(2)];

    CHECK(v0 == i0);
    CHECK(v1 == i1);
    CHECK(v2 == i2);

    CHECK(v0 == j0);
    CHECK(v1 == j1);
    CHECK(v2 == j2);

    CHECK(v0 == k0);
    CHECK(v1 == k1);
    CHECK(v2 == k2);
}
