#include <doctest.hh>

#include <glm/glm.hpp>
#include <typed-geometry/tg-std.hh>

#include <polymesh/Mesh.hh>
#include <polymesh/algorithms/components.hh>
#include <polymesh/properties.hh>
#include <polymesh/objects.hh>

namespace
{
template <class Pos3>
void test_cube(pm::Mesh const& m, pm::vertex_attribute<Pos3> const& pos)
{
    // topological
    CHECK(m.vertices().size() == 8);
    CHECK(m.faces().size() == 6);
    CHECK(m.edges().size() == 4 * 3);
    CHECK(m.halfedges().size() == 4 * 3 * 2);

    CHECK(!is_triangle_mesh(m));
    CHECK(is_quad_mesh(m));
    for (auto f : m.faces())
        CHECK(f.vertices().size() == 4);
    for (auto v : m.vertices())
    {
        CHECK(v.faces().size() == 3);
        CHECK(valence(v) == 3);
    }

    CHECK(is_closed_mesh(m));
    CHECK(euler_characteristic(m) == 2);

    int v_comps;
    int f_comps;
    vertex_components(m, &v_comps);
    face_components(m, &f_comps);
    CHECK(v_comps == 1);
    CHECK(f_comps == 1);

    // attributes
    auto vnormals = vertex_normals_by_area(pos);
    auto fnormals = face_normals(pos);
    auto fareas = m.faces().map([&](auto f) { return face_area(f, pos); });
    auto edge_lengths = m.edges().make_attribute<float>();

    for (auto e : m.edges())
    {
        auto p0 = pos[e.vertexA()];
        auto p1 = pos[e.vertexB()];
        edge_lengths[e] = distance(p0, p1);
    }

    for (auto f : m.faces())
    {
        auto n = fnormals[f];
        auto an = abs(n);
        auto min = tg::min(an.x, an.y, an.z);
        auto max = tg::max(an.x, an.y, an.z);

        if constexpr (std::is_same<Pos3, tg::pos3>::value)
        {
            CHECK(min == min_element(an));
            CHECK(max == max_element(an));
        }

        CHECK(min == 0.0f);
        CHECK(max == 1.0f);

        auto c = face_centroid(f, pos);
        auto cntHalf = 0;
        auto cntFull = 0;
        for (auto i = 0; i < 3; ++i)
        {
            if (c[i] == 0.5f)
                cntHalf++;
            else if (c[i] == 1.0f || c[i] == 0.0f)
                cntFull++;
        }
        CHECK(cntHalf == 2);
        CHECK(cntFull == 1);
    }

    for (auto h : m.halfedges())
    {
        auto d = edge_dir(h, pos);
        auto ad = abs(d);
        auto min = tg::min(ad.x, ad.y, ad.z);
        auto max = tg::max(ad.x, ad.y, ad.z);

        CHECK(min == 0.0f);
        CHECK(max == 1.0f);

        CHECK(edge_length(h, pos) == edge_length(h.edge(), pos));
    }

    for (auto v : m.vertices())
    {
        auto vn = vnormals[v];

        for (auto f : v.faces())
        {
            CHECK(f.is_valid());
            CHECK(fareas[f] == 1.0f);

            auto fn = face_normal(f, pos);

            CHECK(dot(vn, fn) > 0.0f);
        }
    }

    for (auto e : m.edges())
    {
        CHECK(edge_lengths[e] == 1.0f);
    }

    auto [pmin, pmax] = pos.minmax();
    CHECK(pmin == Pos3(0, 0, 0));
    CHECK(pmax == Pos3(1, 1, 1));
}
} // namespace

TEST_CASE("Objects.CubePropertiesTG")
{
    pm::Mesh m;
    auto pos = m.vertices().make_attribute<tg::pos3>();

    pm::objects::add_cube(m, [&](pm::vertex_handle v, float x, float y, float z) {
        // assign position
        pos[v] = {x, y, z};
    });

    test_cube(m, pos);

    auto ll = low_level_api(m);
    CHECK(ll.vertex_attribute_count() == 1);
    CHECK(ll.edge_attribute_count() == 0);
    CHECK(ll.face_attribute_count() == 0);
    CHECK(ll.halfedge_attribute_count() == 0);

    m.assert_consistency();

    {
        pm::Mesh m2;
        auto pos2 = m2.vertices().make_attribute<tg::pos3>();

        m2.copy_from(m);
        pos2.copy_from(pos);

        test_cube(m2, pos2);

        m2.assert_consistency();
    }
}

TEST_CASE("Objects.CubePropertiesGlm")
{
    pm::Mesh m;
    auto pos = m.vertices().make_attribute<glm::vec3>();

    pm::objects::add_cube(m, [&](pm::vertex_handle v, float x, float y, float z) {
        // assign position
        pos[v] = {x, y, z};
    });

    test_cube(m, pos);

    {
        pm::Mesh m2;
        auto pos2 = m2.vertices().make_attribute<glm::vec3>();

        m2.copy_from(m);
        pos2.copy_from(pos);

        test_cube(m2, pos2);

        m2.assert_consistency();
    }
}
