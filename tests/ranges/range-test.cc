#include <doctest.hh>

#include <map>

#include <polymesh/Mesh.hh>
#include <polymesh/objects/cube.hh>
#include <polymesh/properties.hh>

#include <typed-geometry/tg.hh>

TEST_CASE("Ranges.Basics")
{
    pm::Mesh m;
    auto pos = m.vertices().make_attribute<tg::pos3>();
    pm::objects::add_cube(m, pos);

    CHECK(pos.avg() == tg::pos3(0.5f));
    CHECK(pos.min() == tg::pos3(0));
    CHECK(pos.max() == tg::pos3(1));

    CHECK(m.vertices().avg(pos) == tg::pos3(0.5f));
    CHECK(m.vertices().min(pos) == tg::pos3(0));
    CHECK(m.vertices().max(pos) == tg::pos3(1));

    CHECK(m.vertices().where([&](pm::vertex_handle v) { return pos[v].x > 0.5f; }).count() == 4);

    struct face_value
    {
        float area;
        float boundary_length;
        tg::pos3 centroid;
    };

    auto fvals = m.faces().map([&](pm::face_handle f) {
        face_value v;
        v.area = face_area(f, pos);
        v.boundary_length = f.edges().sum([&](pm::edge_handle e) { return edge_length(e, pos); });
        v.centroid = f.vertices().avg(pos);
        return v;
    });

    // TODO !!!!!!!!!
    // m.vertices().map(&tg::pos3::y);
    //
    // struct foo {
    //     float bar();
    //     float baz(int);
    //     float baz2(pm::vertex_attribute<int> const&);
    // };
    // auto f = m.vertices().make_attribute<foo>();
    // auto ia = m.vertices().make_attribute<int>();
    // f.map(&foo::bar);
    // f.map(&foo::baz, 7);
    // f.map(&foo::baz, ia);
    // f.map(&foo::baz2, ia);
    // f.map_to(ia, ...);
    // auto el = m.edges().map(pm::edge_length, pos);
    // m.edges().map([&](auto e) { return edge_length(e, pos); });

    pos = m.vertices().map([&](pm::vertex_handle v) { return v.adjacent_vertices().avg(pos); });
}

TEST_CASE("Ranges.Random")
{
    pm::Mesh m;
    auto pos = m.vertices().make_attribute<tg::pos3>();
    pm::objects::add_cube(m, pos);

    tg::rng rng;
    auto f = m.faces().random(rng);

    std::map<pm::vertex_index, int> cnts;
    auto cnt = 10000;
    for (auto i = 0; i < cnt; ++i)
        cnts[f.vertices().random(rng)]++;

    for (auto const& kvp : cnts)
        CHECK(kvp.second >= cnt / 4 * 0.9);
}
