#include <doctest.hh>

#include <vector>

#include <polymesh/span.hh>

TEST_CASE("pm::span")
{
    std::vector<int> v = {1, 2, 3};
    auto s = polymesh::span<int const>(v);
    CHECK(s.size() == 3);
    CHECK(s[0] == 1);
    CHECK(s[1] == 2);
    CHECK(s[2] == 3);

    auto test = [](polymesh::span<int const> s) {
        CHECK(s.size() == 2);
        CHECK(s[0] == 5);
        CHECK(s[1] == 5);
    };
    test(std::vector<int>(2, 5));
}
