#include <doctest.hh>

#include <imgui/imgui.h>

#include <glow-extras/viewer/view.hh>
#include <polymesh/pm.hh>

TEST_CASE("glow::viewer test")
{
    // test should not actually show something
    return;

    pm::Mesh m;
    auto pos = pm::vertex_attribute<tg::pos3>(m);

    pm::objects::add_quad(
        m,
        [&](auto v, float x, float y) {
            pos[v] = {x, 0, y};
        },
        32, 32);

    float noise = 0.01f;

    glow::viewer::interactive([&](auto) {
        auto changed = ImGui::SliderFloat("noise", &noise, 0.0f, 0.1f);

        tg::rng rng;
        for (auto& p : pos)
            p.y = gaussian(rng, 0.f, noise);

        auto v = gv::view(pos, gv::clear_accumulation(changed));
        gv::view(gv::lines(pos).line_width_world(0.001f));
    });
}
